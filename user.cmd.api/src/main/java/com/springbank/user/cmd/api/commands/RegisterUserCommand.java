package com.springbank.user.cmd.api.commands;

import com.springbank.user.core.models.User;
import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
public class RegisterUserCommand
{
   @TargetAggregateIdentifier
   // In order for AXON to know which instance of an aggregate type should handle the cmd message,
   // the field carrying the aggregate identifier in the cmd object must be anotaed with @TargetAggregateIdentifier
   private String id;

   // A cmd should carry the info required to take action based on the expressed intent. In this case, we need the user info in order
   // to register it
   private User user;
}
