package com.springbank.user.cmd.api.aggregates;

import com.springbank.user.cmd.api.commands.RegisterUserCommand;
import com.springbank.user.cmd.api.commands.RemoveUserCommand;
import com.springbank.user.cmd.api.commands.UpdateUserCommand;
import com.springbank.user.cmd.api.security.PasswordEncoder;
import com.springbank.user.cmd.api.security.PasswordEncoderImpl;
import com.springbank.user.core.events.UserRegisteredEvent;
import com.springbank.user.core.events.UserRemovedEvent;
import com.springbank.user.core.events.UserUpdatedEvent;
import com.springbank.user.core.models.User;
import lombok.var;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.UUID;

@Aggregate
public class UserAggregate
{
   @AggregateIdentifier
   // This annotation marks the field as the reference point to the user aggregate object that AXON uses
   // to know which aggregate a given command is targeting.
   // Remember, we mark the ID of our command objects with at TargetAggregateIdentifier.
   private String id;

   private User user;

   private final PasswordEncoder passwordEncoder;

   // Exxon requires that each aggregate object contains two constructors: one, taking in no arguments
   // or parameters and another that takes in a command object as a parameter
   public UserAggregate()
   {
      passwordEncoder = new PasswordEncoderImpl();
   }

   @CommandHandler
   // This is the command that creates the aggregate.
   public UserAggregate(RegisterUserCommand command)
   {
      var newUser = command.getUser();
      newUser.setId(command.getId());
      passwordEncoder = new PasswordEncoderImpl();
      var password = passwordEncoder.hashPassword(newUser.getAccount().getPassword());
      newUser.getAccount().setPassword(password);
      var event = UserRegisteredEvent.builder().id(command.getId()).user(newUser).build();

      // store the event in the event store and publish it into the event bus.
      AggregateLifecycle.apply(event);
   }

   @CommandHandler
   public void handle(UpdateUserCommand command)
   {
      var updatedUser = command.getUser();
      updatedUser.setId(command.getId());
      var password = passwordEncoder.hashPassword(updatedUser.getAccount().getPassword());
      updatedUser.getAccount().setPassword(password);

      var event = UserUpdatedEvent.builder().id(UUID.randomUUID().toString()).user(updatedUser).build();

      // store the event in the event store and publish it into the event bus.
      AggregateLifecycle.apply(event);
   }

   @CommandHandler
   public void handle(RemoveUserCommand command)
   {
      var event = new UserRemovedEvent();
      event.setId(command.getId());

      // store the event in the event store and publish it into the event bus.
      AggregateLifecycle.apply(event);
   }

   @EventSourcingHandler
   public void on(UserRegisteredEvent event)
   {
      this.id = event.getId();
      this.user = event.getUser();
   }

   @EventSourcingHandler
   public void on(UserUpdatedEvent event)
   {
      this.user = event.getUser();
   }

   @EventSourcingHandler
   public void on(UserRemovedEvent event)
   {
      AggregateLifecycle.markDeleted();
   }
}
