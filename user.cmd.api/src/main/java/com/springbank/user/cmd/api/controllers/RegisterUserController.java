package com.springbank.user.cmd.api.controllers;

import com.springbank.user.cmd.api.commands.RegisterUserCommand;
import com.springbank.user.cmd.api.dto.RegisterUserResponse;
import lombok.var;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "/api/v1/registerUser")
public class RegisterUserController
{
   /**
    * The CommandGateway is a command dispatching mechanism that will dispatch the registered user command
    * and once dispatched, it will invoke the command handling constructor for the register user command and command handling methods for the other user commands.
    */
   private final CommandGateway commandGateway;

   @Autowired
   public RegisterUserController(CommandGateway commandGateway)
   {
      this.commandGateway = commandGateway;
   }

   @PostMapping
   @PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
   public ResponseEntity<RegisterUserResponse> register(@RequestBody RegisterUserCommand command)
   {
      var id = UUID.randomUUID().toString();
      command.setId(id);
      try
      {
         commandGateway.send(command);

         return new ResponseEntity<>(new RegisterUserResponse(id, "User successfully created."), HttpStatus.CREATED);
      }
      catch (Exception e)
      {
         var message = "Error while processing register user request for id " + command.getId();
         System.out.println(e.toString());
         return new ResponseEntity<>(new RegisterUserResponse(id, message), HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
