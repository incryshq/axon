package com.springbank.user.cmd.api.controllers;

import com.springbank.user.cmd.api.commands.UpdateUserCommand;
import com.springbank.user.core.dto.BaseResponse;
import com.springbank.user.cmd.api.dto.RegisterUserResponse;
import lombok.var;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/updateUser")
public class UpdateUserController
{
   private final CommandGateway commandGateway;

   @Autowired
   public UpdateUserController(CommandGateway commandGateway)
   {
      this.commandGateway = commandGateway;
   }

   @PutMapping(path = "/{id}")
   @PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
   public ResponseEntity<BaseResponse> updateUser(@PathVariable(value = "id") String id, @RequestBody UpdateUserCommand command)
   {
      try
      {
         command.setId(id);
         commandGateway.send(command);
         return new ResponseEntity<>(new BaseResponse("user successfully updated"), HttpStatus.OK);
      }
      catch (Exception e)
      {
         var message = "Error while processing update user request for id " + command.getId();
         System.out.println(e.toString());
         return new ResponseEntity<>(new RegisterUserResponse(id, message), HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
