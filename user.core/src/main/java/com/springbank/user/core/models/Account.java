package com.springbank.user.core.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account
{
   @Size(min=2, message = "username must have at least 2 characters")
   private String username;

   @Size(min=8, message = "password must have at least 8 characters")
   private String password;

   @NotNull(message = "At least one role is required")
   private List<Role> roles;
}
