package com.springbank.user.query.api.handlers;

import com.springbank.user.core.events.UserRegisteredEvent;
import com.springbank.user.core.events.UserRemovedEvent;
import com.springbank.user.core.events.UserUpdatedEvent;
import com.springbank.user.query.api.repositories.UserRepository;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Service;

/**
 * A processing group is similar to a consumer group, which basically means that when a consumer, event
 * handler in our case, consumes an event, AXON will track the offset to make sure that within a given processing
 * group that you will always consume the latest event.
 * AXON basically manages the consumed offset for each processing group separately.
 */

@Service
@ProcessingGroup("user-group")
public class UserEventHandlerImpl implements UserEventHandler
{
   private final UserRepository userRepository;

   public UserEventHandlerImpl(UserRepository userRepository)
   {
      this.userRepository = userRepository;
   }

   /**
    * event handlers basically defines the business logic to be
    * performed when an event is received or consumed from the event bus.
    * @param event
    */
   @EventHandler
   @Override
   public void on(UserRegisteredEvent event)
   {
      userRepository.save(event.getUser());
   }

   @EventHandler
   @Override
   public void on(UserUpdatedEvent event)
   {
      userRepository.save(event.getUser());
   }

   @EventHandler
   @Override
   public void on(UserRemovedEvent event)
   {
      userRepository.deleteById(event.getId());
   }
}
