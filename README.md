Create a docker network

`docker network create --attachable -d overlay springbankNet`

Install mongodb (docker)

`docker run -it -d --name mongo-container \\n-p 27017:27017 --network springbankNet \\n--restart always \\n-v mongodb_data_container:/data/db \\nmongo:latest`

Install AXON server (docker)

`docker run -d --name axon-server \\n-p 8024:8024 -p 8124:8124 \\n--network springbankNet \\n--restart always axoniq/axonserver:latest`

